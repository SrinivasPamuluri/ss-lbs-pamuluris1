public aspect HelloAspect{
	java.util.Date date = new java.util.Date();
	pointcut greeting(): call(* Hello.greeting(..));
	before(): greeting(){
          System.out.println("AOP test:  from SRINIVASA.This is before is invoked"); 
    	}
	after(): greeting(){
	System.out.println("This is after Hello.greeting(..) is invoked. Time: " + date);
    	}
}
