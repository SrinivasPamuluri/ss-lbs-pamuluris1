public aspect ShoppingCartAspect{
	java.util.Date date = new java.util.Date();
	pointcut setBalance(): call(* Wallet.setBalance (int));
	before(int balance): call(* Wallet.setBalance(int)) && args(balance){
	System.out.print("AOP test from SRINIVAS. this is before calling setBalance");
	System.out.println();

    	}
	after(int balance): call(* Wallet.setBalance(int)) && args(balance){
	System.out.println("by Srinivas, after calling set balance");
	System.out.println("the balance is: " + balance);
	System.out.println("time: " + date +" .");
	System.out.println();
	
    	}
}
