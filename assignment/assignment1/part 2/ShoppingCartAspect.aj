public aspect ShoppingCartAspect{
java.util.Date date  = new java.util.Date();
	pointcut safeWithdraw(int valueToWithdraw): 
	call(int Wallet.safeWithdraw(int)) && args(valueToWithdraw);

    before(int valueToWithdraw): 
    safeWithdraw(valueToWithdraw) {
	   try {

	      Wallet wallet = new Wallet();

	      final int balance = wallet.getBalance();
	        if(balance < valueToWithdraw){
	         System.out.println("insufficient balance to make a purchase");
	        }
        }catch(Exception e){

            }
	     
}
	     after(int price) returning (int withdrawAmount): safeWithdraw(price){
		try{
			Wallet wallet = new Wallet();

			if(withdrawAmount < price){
			System.out.println(" remaining amount in the wallet");
			wallet.safeDeposit(withdrawAmount);
		    }

		}
		catch(Exception e){
		}
	}	

} 